package com.spring.cloud.service.discovery.eureka

import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEmpty
import assertk.assertions.isNotNull
import com.netflix.appinfo.InstanceInfo.Builder.newBuilder
import com.netflix.discovery.EurekaClient
import com.netflix.discovery.EurekaClientConfig
import com.netflix.discovery.shared.Application
import com.netflix.discovery.shared.Applications
import com.spring.cloud.config.service.discovery.eureka.RequiredServiceVersionProperties
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.cloud.client.ServiceInstance
import java.util.UUID

class EurekaVersionAwareServletDiscoveryClientTest {

    private val requiredServiceVersionProperties = mockk<RequiredServiceVersionProperties>()
    private val eurekaClient = mockk<EurekaClient>()
    private val eurekaClientConfig = mockk<EurekaClientConfig>()
    private val applications = mockk<Applications>()

    private val underTest = EurekaVersionAwareServletDiscoveryClient(
        requiredServiceVersionProperties,
        eurekaClient,
        eurekaClientConfig
    )

    companion object {
        private const val VERSION_METADATA_FIELD_NAME = "version"
    }

    @Test
    fun `given there are known services with their required versions in service discovery when services list is requested then filtered list of services is returned`() {
        val registeredApplication1Name = "service-a"
        val registeredApplication1RequiredVersion = "1"
        val registeredApplication1 = Application().apply {
            name = registeredApplication1Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to registeredApplication1RequiredVersion))
                    .build()
            )
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "2"))
                    .build()
            )
        }
        val registeredApplication2Name = "service-b"
        val registeredApplication2RequiredVersion = "2"
        val registeredApplication2 = Application().apply {
            name = registeredApplication2Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "1"))
                    .build()
            )
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to registeredApplication2RequiredVersion))
                    .build()
            )
        }
        every { eurekaClient.applications } returns applications
        every { applications.registeredApplications } returns listOf(registeredApplication1, registeredApplication2)
        every { requiredServiceVersionProperties.containsKey(registeredApplication1Name) } returns true
        every { requiredServiceVersionProperties[registeredApplication1Name] } returns registeredApplication1RequiredVersion
        every { requiredServiceVersionProperties.containsKey(registeredApplication2Name) } returns true
        every { requiredServiceVersionProperties[registeredApplication2Name] } returns registeredApplication2RequiredVersion

        assertThat(underTest.services).containsExactlyInAnyOrder(registeredApplication1Name, registeredApplication2Name)

        verify {
            eurekaClient.applications
            applications.registeredApplications
            requiredServiceVersionProperties.containsKey(registeredApplication1Name)
            requiredServiceVersionProperties[registeredApplication1Name]
            requiredServiceVersionProperties.containsKey(registeredApplication2Name)
            requiredServiceVersionProperties[registeredApplication2Name]
        }
    }

    @Test
    fun `given there are known services with no required version in service discovery when services list is requested then empty list of services is returned`() {
        val registeredApplication1Name = "service-a"
        val registeredApplication1RequiredVersion = "1"
        val registeredApplication1 = Application().apply {
            name = registeredApplication1Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "3"))
                    .build()
            )
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "2"))
                    .build()
            )
        }
        val registeredApplication2Name = "service-b"
        val registeredApplication2RequiredVersion = "2"
        val registeredApplication2 = Application().apply {
            name = registeredApplication2Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "1"))
                    .build()
            )
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "3"))
                    .build()
            )
        }
        every { eurekaClient.applications } returns applications
        every { applications.registeredApplications } returns listOf(registeredApplication1, registeredApplication2)
        every { requiredServiceVersionProperties.containsKey(registeredApplication1Name) } returns true
        every { requiredServiceVersionProperties[registeredApplication1Name] } returns registeredApplication1RequiredVersion
        every { requiredServiceVersionProperties.containsKey(registeredApplication2Name) } returns true
        every { requiredServiceVersionProperties[registeredApplication2Name] } returns registeredApplication2RequiredVersion

        assertThat(underTest.services).isEmpty()

        verify {
            eurekaClient.applications
            applications.registeredApplications
            requiredServiceVersionProperties.containsKey(registeredApplication1Name)
            requiredServiceVersionProperties[registeredApplication1Name]
            requiredServiceVersionProperties.containsKey(registeredApplication2Name)
            requiredServiceVersionProperties[registeredApplication2Name]
        }
    }

    @Test
    fun `given there are both known as well as unknown services in service discovery when services list is requested then filtered list of services is returned`() {
        val registeredApplication1Name = "service-a"
        val registeredApplication1RequiredVersion = "1"
        val registeredApplication1 = Application().apply {
            name = registeredApplication1Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to registeredApplication1RequiredVersion))
                    .build()
            )
        }
        val registeredApplication2Name = "service-b"
        val registeredApplication2 = Application().apply {
            name = registeredApplication2Name.uppercase()
            addInstance(
                newBuilder()
                    .setInstanceId(UUID.randomUUID().toString())
                    .setAppName(name)
                    .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "1"))
                    .build()
            )
        }
        every { eurekaClient.applications } returns applications
        every { applications.registeredApplications } returns listOf(registeredApplication1, registeredApplication2)
        every { requiredServiceVersionProperties.containsKey(registeredApplication1Name) } returns true
        every { requiredServiceVersionProperties[registeredApplication1Name] } returns registeredApplication1RequiredVersion
        every { requiredServiceVersionProperties.containsKey(registeredApplication2Name) } returns false

        assertThat(underTest.services).containsExactly(registeredApplication1Name)

        verify {
            eurekaClient.applications
            applications.registeredApplications
            requiredServiceVersionProperties.containsKey(registeredApplication1Name)
            requiredServiceVersionProperties[registeredApplication1Name]
            requiredServiceVersionProperties.containsKey(registeredApplication2Name)
        }
    }

    @Test
    fun `given there are known instances with required as well as different versions in service discovery when instances list is requested then instances with required version are returned`() {
        val applicationName = "service-a"
        val requiredVersion = "1"
        val instanceWithRightVersion1 = newBuilder()
            .setInstanceId(UUID.randomUUID().toString())
            .setAppName(applicationName)
            .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to requiredVersion))
            .build()
        val instanceWithRightVersion2 = newBuilder()
            .setInstanceId(UUID.randomUUID().toString())
            .setAppName(applicationName)
            .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to requiredVersion))
            .build()
        val instanceWithWrongVersion = newBuilder()
            .setInstanceId(UUID.randomUUID().toString())
            .setAppName(applicationName)
            .setMetadata(mutableMapOf(VERSION_METADATA_FIELD_NAME to "2"))
            .build()
        val instanceWithNoVersion = newBuilder()
            .setInstanceId(UUID.randomUUID().toString())
            .setAppName(applicationName)
            .build()
        every {
            eurekaClient.getInstancesByVipAddress(applicationName, false)
        } returns listOf(
            instanceWithRightVersion1,
            instanceWithRightVersion2,
            instanceWithWrongVersion,
            instanceWithNoVersion
        )
        every { requiredServiceVersionProperties.containsKey(applicationName) } returns true
        every { requiredServiceVersionProperties[applicationName] } returns requiredVersion

        val instances = underTest.getInstances(applicationName)

        assertThat(instances).isNotNull()
        assertThat(instances.map(ServiceInstance::getInstanceId))
            .containsExactlyInAnyOrder(instanceWithRightVersion1.instanceId, instanceWithRightVersion2.instanceId)

        verify {
            eurekaClient.getInstancesByVipAddress(applicationName, false)
            requiredServiceVersionProperties.containsKey(applicationName)
            requiredServiceVersionProperties[applicationName]
        }
    }

    @Test
    fun `given there are not known instances in service discovery when instances list is requested then empty list of instances is returned`() {
        val applicationName = "service-a"
        every { eurekaClient.getInstancesByVipAddress(applicationName, false) } returns emptyList()

        assertThat(underTest.getInstances(applicationName)).isEmpty()

        verify {
            eurekaClient.getInstancesByVipAddress(applicationName, false)
        }
    }
}
