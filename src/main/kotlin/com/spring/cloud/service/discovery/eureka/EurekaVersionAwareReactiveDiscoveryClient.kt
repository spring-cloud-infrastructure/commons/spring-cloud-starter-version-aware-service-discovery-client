package com.spring.cloud.service.discovery.eureka

import com.netflix.appinfo.InstanceInfo
import com.netflix.discovery.EurekaClient
import com.netflix.discovery.EurekaClientConfig
import com.netflix.discovery.shared.Application
import com.netflix.discovery.shared.Applications
import com.spring.cloud.config.service.discovery.eureka.RequiredServiceVersionProperties
import mu.KotlinLogging.logger
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type.REACTIVE
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance
import org.springframework.cloud.netflix.eureka.reactive.EurekaReactiveDiscoveryClient
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import java.util.Locale

@Service
@ConditionalOnWebApplication(type = REACTIVE)
@ConditionalOnProperty(name = ["eureka.client.enabled"], matchIfMissing = true)
class EurekaVersionAwareReactiveDiscoveryClient(
    private val requiredServiceVersionProperties: RequiredServiceVersionProperties,
    private val eurekaClient: EurekaClient,
    eurekaClientConfig: EurekaClientConfig,
) : EurekaReactiveDiscoveryClient(eurekaClient, eurekaClientConfig) {

    private val logger = logger { }

    companion object {
        private const val VERSION_METADATA_FIELD_NAME = "version"
    }

    override fun description(): String {
        return "Spring Cloud Eureka Version Aware Reactive Discovery Client"
    }

    override fun getInstances(serviceName: String): Flux<ServiceInstance> =
        eurekaClient.getInstancesByVipAddress(serviceName, false)
            .toFlux()
            .filter(::isServiceInstanceKnownAndIsInRequiredVersion)
            .map(::EurekaServiceInstance)

    override fun getServices(): Flux<String> = eurekaClient.applications
        .toMono()
        .flatMapIterable(Applications::getRegisteredApplications)
        .filter { it.instances.any(::isServiceInstanceKnownAndIsInRequiredVersion) }
        .filter { isServiceKnown(it.name) }
        .map(Application::getName)
        .map(String::lowercase)

    private fun isServiceInstanceKnownAndIsInRequiredVersion(instance: InstanceInfo): Boolean =
        isServiceKnown(instance.appName) &&
            hasServiceInstanceVersion(instance) &&
            getRequiredServiceVersion(instance) == getServiceInstanceVersion(instance)

    private fun isServiceKnown(serviceName: String): Boolean = when {
        requiredServiceVersionProperties.containsKey(serviceName.lowercase(Locale.getDefault())) -> true
        else -> {
            logger.warn { "Service: [${serviceName.lowercase(Locale.getDefault())}] with undefined required version found by service discovery" }
            false
        }
    }

    private fun hasServiceInstanceVersion(instance: InstanceInfo): Boolean = when {
        instance.metadata.containsKey(VERSION_METADATA_FIELD_NAME) -> true
        else -> {
            logger.warn { "Service: [${instance.appName}] has no [$VERSION_METADATA_FIELD_NAME] metadata information" }
            false
        }
    }

    private fun getRequiredServiceVersion(instance: InstanceInfo): String =
        requiredServiceVersionProperties[instance.appName.lowercase(Locale.getDefault())]!!

    private fun getServiceInstanceVersion(instance: InstanceInfo): String =
        instance.metadata[VERSION_METADATA_FIELD_NAME]!!
}
