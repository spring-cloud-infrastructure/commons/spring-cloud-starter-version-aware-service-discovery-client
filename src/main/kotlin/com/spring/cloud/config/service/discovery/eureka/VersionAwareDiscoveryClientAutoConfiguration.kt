package com.spring.cloud.config.service.discovery.eureka

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationPropertiesScan
@ConditionalOnProperty(name = ["eureka.client.enabled"], matchIfMissing = true)
class VersionAwareDiscoveryClientAutoConfiguration
