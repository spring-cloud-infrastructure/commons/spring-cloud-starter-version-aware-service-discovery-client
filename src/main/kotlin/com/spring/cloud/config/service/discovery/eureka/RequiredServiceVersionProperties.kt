package com.spring.cloud.config.service.discovery.eureka

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("services.required-versions")
class RequiredServiceVersionProperties : HashMap<String, String>()
