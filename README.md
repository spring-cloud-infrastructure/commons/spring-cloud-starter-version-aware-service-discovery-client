# Spring Cloud Starter Version Aware Service Discovery Client

Provides Spring auto-configuration for version aware Service Discovery client (Eureka).

## Use in a project

- Add dependency:

```kotlin
implementation("com.spring.cloud:spring-cloud-starter-version-aware-service-discovery-client:<VERSION>")
```

- Define service version property in eureka instance config:

```yaml
eureka:
  instance:
    metadataMap:
      version: version-of-your-service # e.g. 1.0.0-SNAPSHOT
```

- Define service required versions properties:

```yaml
services:
  required-versions: # map of service-name and required version pairs
    service-name: required-version
```
